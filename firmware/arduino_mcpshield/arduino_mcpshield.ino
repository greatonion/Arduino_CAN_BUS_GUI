

#define SERIAL_SPEED 115200
#define BUFFER_SIZE 50
String devID = "MCP_shield_v0.1";


struct TxCan{
  unsigned long timestamp;
  unsigned int id;
  byte ex = 0;
  byte DLC = 8;
  byte D0;
  byte D1;
  byte D2;
  byte D3;
  byte D4;
  byte D5;
  byte D6;
  byte D7;
  
};

union  
{
	unsigned long fval;
	byte bval[4];
} floatAsBytes;

union
{
	unsigned int ival;
	byte bval[4];
} uintAsBytes;



int bufferLastIdx= 0;
int bufferCount = 0;
int bufferOverflow = 0;



struct ProtocoolBytes{
  byte startb = 17;
  byte stopb = 33;
  
} pb;

struct ProtocoolComands{
  int returnId = 1;
  int getCanSpeed = 2;
  int sendBuffer = 3;
  int transmitCanFrame = 4;
  int setCanSpeed = 5;
}pc;

struct CanSpeeds {
	int can50k = 1;
	int can100k = 2;
	int can125k = 3;
	int can250k = 4;
	int can500k = 5;
	int can1000k = 6;

} cspeed;

TxCan tx_buffer[BUFFER_SIZE];


void SendTxCanFrame(TxCan &frame) {
	
	floatAsBytes.fval = frame.timestamp;
	uintAsBytes.ival = frame.id;
	byte toSend[] = {floatAsBytes.bval[3],floatAsBytes.bval[2],
		floatAsBytes.bval[0],floatAsBytes.bval[1],
		uintAsBytes.bval[3],uintAsBytes.bval[2],uintAsBytes.bval[1],uintAsBytes.bval[0],
		frame.ex,frame.DLC,frame.D0,frame.D1,frame.D2,frame.D3,frame.D4,frame.D5,
		frame.D6, frame.D7 };
	
	Serial.write(toSend, 18);

}



int getBufferNextIdx(){
   bufferLastIdx+=1;
  if (bufferLastIdx >= BUFFER_SIZE ){
    bufferLastIdx = 0;    
   }
   bufferCount += 1;
   if (bufferCount > BUFFER_SIZE) {
    bufferOverflow = 1;
    bufferCount = BUFFER_SIZE;
     
   }
   return bufferLastIdx;
   
  
  
}

void GenerateSomeCanFrames(){
  for(int i=0; i<60;i++){
    int idx = getBufferNextIdx();
    
    TxCan _canFrame;
    _canFrame.timestamp = micros();
    _canFrame.id = i*66;
	_canFrame.ex = i % 2;
    _canFrame.D0 = idx;
    _canFrame.D1 = i;
    _canFrame.D2 = i;
    _canFrame.D3 = i;
    _canFrame.D4 = i;
    _canFrame.D5 = i;
    _canFrame.D6 = i;
    _canFrame.D7 = i;
    tx_buffer[idx] = _canFrame;

	delay(1);
  }
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(SERIAL_SPEED);
  GenerateSomeCanFrames();
}



int incomingByte;
unsigned int bufIndx;

void SendID(){
  //Serial.write(6);
  String toSend = "{:devID:"+devID+":}\n";
  Serial.print(toSend);
}

void SendBuffer(){

	int _bufferCount = bufferCount;
	int _bufferLastId = bufferLastIdx;
	

	String toSend = "{:frames:" + (String)_bufferCount + ":" + (String)bufferOverflow + ":}\n";
	Serial.print(toSend);

	bufferOverflow = 0;
	bufferCount = 0;

	//c = 8
	// 0 , 1 , 2, 3, 4, 5, 6l, 7, 8, 9
	//Send them all 
	int idx = _bufferLastId - _bufferCount;
	if (idx < 0) {
		idx = BUFFER_SIZE - idx;
	}
	
	for (int i=0; i < _bufferCount; i++) {
		idx += 1;
		if (idx >= BUFFER_SIZE) {
			idx = 0;
			
		}
		SendTxCanFrame(tx_buffer[idx]);

	}
}


void SetCanSpeed(byte enumSpeed) {
	String ConfiguredSpeed;
	String stat = "OK";
	if (enumSpeed == cspeed.can50k) ConfiguredSpeed = "50k";
	else if (enumSpeed == cspeed.can100k) ConfiguredSpeed = "100k";
	else if (enumSpeed == cspeed.can125k) ConfiguredSpeed = "125k";
	else if (enumSpeed == cspeed.can250k) ConfiguredSpeed = "250k";
	else if (enumSpeed == cspeed.can500k) ConfiguredSpeed = "500k";
	else if (enumSpeed == cspeed.can1000k) ConfiguredSpeed = "1000k";
	else { 
		ConfiguredSpeed = "NOK"; 
		stat = "NOK";
	}


	String toSend = "{:CanSetup:" + stat + ":" + ConfiguredSpeed + ":}\n";
	Serial.print(toSend);


}


void SendCanSpeed() {

	int _canSpeed = 3;
	//GetCanSpeed() //TODO
	String canspeed;
	if (_canSpeed == cspeed.can50k) canspeed = "50k";
	else if (_canSpeed == cspeed.can100k) canspeed = "100k";
	else if (_canSpeed == cspeed.can125k) canspeed = "125k";
	else if (_canSpeed == cspeed.can250k) canspeed = "250k";
	else if (_canSpeed == cspeed.can500k) canspeed = "500k";
	else if (_canSpeed == cspeed.can1000k) canspeed = "1000k";
	else {
		canspeed == "Unsuported";
	}
	String toSend = "{:CanSpeed:" + canspeed + ":" + (String)_canSpeed + ":}\n";
	Serial.print(toSend);


}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available() ){
     incomingByte = Serial.read();
     if (incomingByte == pb.startb){
        delay(3);
        //Select function
        incomingByte = Serial.read();
        if (incomingByte == pc.returnId){
           SendID();
        }
        else if (incomingByte == pc.sendBuffer){
           SendBuffer();
        }
		else if(incomingByte == pc.getCanSpeed)
		{
			SendCanSpeed();
		}
		else if(incomingByte == pc.setCanSpeed)
		{
			delay(3);
			byte can_speed = Serial.read();
			SetCanSpeed(can_speed);
		}
     }
     
  }
  

}
