﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace can_bus_gui
{

    static class SerialComands
    {
        public static byte StartBit = 17;
        public static byte StopBit = 33;
        public static byte[] getID = new byte[] { StartBit, 1, StopBit };
        public static byte[] getBuffer = new byte[] { StartBit, 3, StopBit };
        public static byte[] setCanSpeed = new byte[] { StartBit, 5, 0, StopBit };
        public static int FrameBytesCount = 18;
        public static byte[] getCANspeed = new byte[] { StartBit, 2, StopBit };
        public enum CANspeeds {can50k = 1, can100k, can125k, can250k, can500k, can1000k,unsuported  };
    

    }

    class ComPortsName
    {
        public string comPortName;
        public string deviceName;
    }



    class ArduinoWrapper
    {
        static int ComPortBaudrate = 230400;
        //Singleton
        private SerialPort port;

        private static ArduinoWrapper instance;
               
        private ArduinoWrapper() { }

        public static ArduinoWrapper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ArduinoWrapper();
                }
                return instance;
            }
        }
        //

        void ReturnCanParameters()
        {

        }

        public bool ConfigureBaudrate(SerialComands.CANspeeds cA)
        {
            byte[] sendFrame = SerialComands.setCanSpeed;
            sendFrame[2] = (byte)cA;
            port.Write(SerialComands.setCanSpeed, 0, SerialComands.setCanSpeed.Length);
            var _out = port.ReadLine();
            Console.WriteLine(_out);
            return (_out.Split(':')[2] == "OK"); 

        }



        public List<ComPortsName> ScanCOMports()
        {

            string[] ports = SerialPort.GetPortNames();

            List<ComPortsName> _ret = new List<ComPortsName>();

            foreach (string portIdx in ports)
            {
                ComPortsName _com = new ComPortsName();
                _com.comPortName = portIdx;
                port = new SerialPort(portIdx, ComPortBaudrate, Parity.None,8, StopBits.One);
                port.ReadTimeout = 100;
                //port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                try
                {
                    port.Open();                                  
                   port.Write(SerialComands.getID, 0, SerialComands.getID.Length);
                    _com.deviceName = port.ReadLine().Split(':')[2];

                }
                catch
                {
                    _com.deviceName = "Can't open";
                }
                finally
                {
                    port.Close();
                }
                


                _ret.Add(_com);

            }



            return _ret;
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //byte[] output;
            //output = (Encoding.ASCII.GetBytes(port.ReadExisting()));
            //Console.WriteLine("newline");
            //Console.WriteLine(Helper.GetHexStringFrom(output));
            Console.WriteLine((port.ReadExisting()));            
        }

        public void ConnectToPort(string COM)
        {
            port = new SerialPort(COM, ComPortBaudrate, Parity.None, 8, StopBits.One);
            port.Open();
        }

        public void ClosePort()
        {
            port.Close();
        }

        public bool isConnected()
        {
            return port.IsOpen;
        }

        CanFrame ReadCanFrame()
        {
            var _can = new CanFrame();
            byte[] _array = new byte[SerialComands.FrameBytesCount];
            for (int i = 0; i < SerialComands.FrameBytesCount; i++)
            {
                _array[i] = (byte)port.ReadByte();
                Console.Write(_array[i] + ", ");
            }

            Console.WriteLine("x");

            _can.Timestamp = (uint)_array[0] * 256 * 256 * 256 + (uint)_array[1] * 256 * 256 + (uint)_array[2] * 256 + (uint)_array[3];
            _can.Id = _array[4] * 256 * 256 * 256 + _array[5] * 256 * 256 + _array[6] * 256 + _array[7];
            _can.ext = (_array[8] == 1);
            _can.Dlc = _array[9];
            _can.D0 = _array[10];
            _can.D1 = _array[11];
            _can.D2 = _array[12];
            _can.D3 = _array[13];
            _can.D4 = _array[14];
            _can.D5 = _array[15];
            _can.D6 = _array[16];
            _can.D7 = _array[17];
            return _can;
        }

        public List<CanFrame> ReadBuffer()
        {
            var _list = new List<CanFrame>();
            port.Write(SerialComands.getBuffer, 0, SerialComands.getBuffer.Length);
            port.ReadTimeout = 100;
            string startFrame = port.ReadLine();
            Console.WriteLine(startFrame);
            if (startFrame.Split(':')[1] == "frames")
            {
                int framesCount;
                int.TryParse(startFrame.Split(':')[2], out framesCount);
                Console.WriteLine("Lines: " + framesCount);
                for(int i = 0; i < framesCount; i++)
                {
                    _list.Add(ReadCanFrame());
                }
            }
            else
            {
                //something went wrong clean buffer
            }
            return _list;

        }

        public String getCurrentComPortName()
        {
            return port.PortName;
        }

        public String GetCanSpeed()
        {
            port.Write(SerialComands.getCANspeed, 0, SerialComands.getCANspeed.Length);
            port.ReadTimeout = 500;
            string response = port.ReadLine();
            Console.WriteLine(response);
            return response;
        }

        void TestCanPort()
        {

        }
    }
}
