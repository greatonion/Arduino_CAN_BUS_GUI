﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace can_bus_gui
{
    class Helper
    {
        public static string GetHexStringFrom(byte[] byteArray)
        {
            return BitConverter.ToString(byteArray);
        }

        public static byte[] ToByteArray(string str)
        {
            return System.Text.Encoding.ASCII.GetBytes(str);
        }

        public static SerialComands.CANspeeds SpeedText2Enum(string SpeedText)
        {
            
            if(SpeedText.Contains("100k")) return SerialComands.CANspeeds.can100k;
            else if(SpeedText.Contains("125k")) return SerialComands.CANspeeds.can125k;
            else if(SpeedText.Contains("250k")) return SerialComands.CANspeeds.can250k;
            else if (SpeedText.Contains("50k")) return SerialComands.CANspeeds.can50k;
            else if(SpeedText.Contains("500k")) return SerialComands.CANspeeds.can500k;
            else if(SpeedText.Contains("1000k")) return SerialComands.CANspeeds.can1000k;
            return SerialComands.CANspeeds.unsuported;
        }

    }
}
