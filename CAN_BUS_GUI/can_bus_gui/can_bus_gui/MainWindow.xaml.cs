﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace can_bus_gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ArduinoWrapper arduino;
        List<CanFrame> CanFramesList = new List<CanFrame>();

        public MainWindow()
        {
            InitializeComponent();
            arduino = ArduinoWrapper.Instance;
            InitialValues();

        }
   
        private void InitialValues()
        {
            /// CAN BAUDRATE INIT BOX
            CanBaudrateComboBox.Items.Clear();


             CanBaudrateComboBox.Items.Add("50kBaud");
            CanBaudrateComboBox.Items.Add("100kBaud");
            CanBaudrateComboBox.Items.Add("125kBaud");
            CanBaudrateComboBox.Items.Add("250kBaud");
            CanBaudrateComboBox.Items.Add("500kBaud");
            CanBaudrateComboBox.Items.Add("1000kBaud");
            CanBaudrateComboBox.SelectedIndex = 0;

            /// COM PORTS
            scanPorts();

            refreshComLabels();
            //DATA GRID CONFIG
            ConfigureDataGrid();


        }

        void ConfigureDataGrid()
        {
            //CanFramesGrid.ItemsSource = GenerateFakeData(25262);
            CanFramesGrid.AutoGenerateColumns = false;
            CanFramesGrid.ItemsSource = CanFramesList;
        }

        private List<CanFrame> GenerateFakeData(int n)
        {
            List<CanFrame> _list = new List<CanFrame>();

            for (int i = 0; i < n; i++)
            {
                CanFrame _frame = new CanFrame();
                _frame.Id = i;
                _frame.Dlc = 8;
                _frame.D0 = 0;
                _frame.D1 = 0;
                _frame.D2 = 0;
                _frame.D3 = 0;
                _frame.D4 = 0;
                _frame.D5 = 0;
                _frame.D6 = 0;
                _frame.D7 = 0;
                _list.Add(_frame);
            }

            return _list;
        }

        void scanPorts()
        {
            comPortsComboBox.Items.Clear();
            List<ComPortsName> _coms = arduino.ScanCOMports();


            foreach (var _com in _coms)
            {
                comPortsComboBox.Items.Add(_com.comPortName + ":" + _com.deviceName);
            }
            comPortsComboBox.SelectedIndex = 0;
            // TO DO: Sort comPortsComboBox.So
        }

        private void RefreshBtn_Click(object sender, RoutedEventArgs e)
        {
            scanPorts();
                        
        }

        private void CanBaudrateComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }


        private void OnPreviewTextInputScope255(object sender, TextCompositionEventArgs e)
        {            
            int fullVal;
            TextBox tb = (sender as TextBox);
            bool tryParse = int.TryParse(tb.Text + e.Text,out fullVal);
            Console.WriteLine(fullVal);
            
            if (tryParse && (fullVal < 255) )
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }


        private void OnPreviewTextInputScope8(object sender, TextCompositionEventArgs e)
        {
            int fullVal;
            TextBox tb = (sender as TextBox);
            bool tryParse = int.TryParse(tb.Text + e.Text, out fullVal);
            Console.WriteLine(fullVal);

            if (tryParse && (fullVal < 9))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void OnPreviewTextInputScope80000(object sender, TextCompositionEventArgs e)
        {
            int fullVal;
            TextBox tb = (sender as TextBox);
            bool tryParse = int.TryParse(tb.Text + e.Text, out fullVal);
            Console.WriteLine(fullVal);

            if (tryParse && (fullVal < 80000))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void SelectAddress(object sender, RoutedEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb.Text != null)
            {
                tb.SelectAll();
            }
        }

        private void CanFramesGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void refreshComLabels()
        {
            if (arduino.isConnected())
            {
                ConnectBtn.Content = "Disconnect";
                ConnStatusLabel.Content = "Connected, " + arduino.getCurrentComPortName();
                RefreshBtn.IsEnabled = false;
                ConfigCANBtn.IsEnabled = true;
            }
            else
            {
                ConnectBtn.Content = "Connect";
                ConnStatusLabel.Content = "Not connected";
                RefreshBtn.IsEnabled = true;
                ConfigCANBtn.IsEnabled = false;
            }
        }

        private void ConnectBtn_Click(object sender, RoutedEventArgs e)
        {
           
            string SelectedComPort = (string)comPortsComboBox.Items[comPortsComboBox.SelectedIndex];
            SelectedComPort = SelectedComPort.Split(':')[0];

            if (!arduino.isConnected())
            {
                arduino.ConnectToPort(SelectedComPort);
                CanSettingsText.Content = arduino.GetCanSpeed().Split(':')[2] + "Baud/s";
            }
            else
            {
                arduino.ClosePort();
            }
            refreshComLabels();

        }

        private void ConfigCANBtn_Click(object sender, RoutedEventArgs e)
        {
            //Console.WriteLine(CanBaudrateComboBox.SelectedItem);
            arduino.ConfigureBaudrate(Helper.SpeedText2Enum(CanBaudrateComboBox.SelectedItem.ToString()));

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var _list = arduino.ReadBuffer();
            foreach (var frame in _list)
            {
                CanFramesList.Add(frame);

            }
            //CanFramesList.
            CanFramesGrid.ItemsSource = null;
            CanFramesGrid.ItemsSource = CanFramesList;
        }
    }
}
