﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace can_bus_gui
{
    class CanFrame
    {
        public UInt32 Timestamp { get; set; }
        public bool ext { get; set; }
        //public bool Dir { get; set; } = true;
        public int Id { get; set; }
        public byte Dlc { get; set; }
        public byte D0 { get; set; }
        public byte D1 { get; set; }
        public byte D2 { get; set; }
        public byte D3 { get; set; }
        public byte D4 { get; set; }
        public byte D5 { get; set; }
        public byte D6 { get; set; }
        public byte D7 { get; set; }
    }
}
